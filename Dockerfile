FROM node:latest
RUN npm install -g npm
RUN ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
COPY api/ /api
WORKDIR  /api
RUN npm install
EXPOSE 3000
ENTRYPOINT [ "npm","start" ]

