const { Pool } = require('pg');
const dotenv = require('dotenv');

dotenv.config();

// ==> Database Connection
const pool = new Pool({
  connectionString: "postgres://api:api123@10.46.0.216:5432/api"
});

pool.on('connect', () => {
  console.log('Database conected!');
});

module.exports = {
  query: (text, params) => pool.query(text, params),
};