const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const app = express();

// Routes of API
const index = require('./routes/index');
const category = require('./routes/category.route');
app.use(express.urlencoded({extended : true}));
app.use(express.json());
app.use(express.json({ type : 'application/vnd.api+json' }));
app.use(cors());
app.use(morgan('combined'))
app.use('/api',index);
app.use('/api',category);
module.exports = app;