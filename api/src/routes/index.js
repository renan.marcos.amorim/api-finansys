const express = require('express');
const router = express.Router();

router.get('/info', (req, res) => {
  res.status(200).send({
    success: 'true',
    message: 'API FINANSYS - OK',
    version: '1.0.0',
  });
});

router.get('/health', (req,res)=> {
    res.status(200).send({
        "Message" : "Api is Health",
        "StatusCode" : 200
    });
});

module.exports = router;