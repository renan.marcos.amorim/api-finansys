const router = require('express-promise-router')();
const { route } = require('.');
const categoryControler = require('../controllers/category.controller');

//Define route about CRUD - Category

router.get('/category', categoryControler.listCategory);
router.get('/category/:id', categoryControler.listCategoryById);
module.exports = router;