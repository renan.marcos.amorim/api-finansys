const db = require("../config/database");

//GET ALL CATEGORY
exports.listCategory = async (req, res)=> {
    const {id,name,description} = req.body;
    const response = await db.query(
        "SELECT * FROM categoria"
    );
    res.status(200).send({body : response.rows})
};

exports.listCategoryById = async (req, res)=> {
    const idCategory = req.params.id;
    const response = await db.query(
        "SELECT * FROM categoria where id = $1",[idCategory]
    );
    if (response.rows != 0){
        res.status(200).send({body : response.rows})
    }else{
        res.status(404).send({
            "ErrorMessage" : "Category Not Found."
        })
    }
};